import { Section } from "@stembord/react-document";
import * as React from "react";
import image from "./image.jpg";

export default (
  <Section title="Introduction">
    <p>Hello, world!</p>
    <div>
      <img style={{ maxWidth: "100%" }} src={image} />
    </div>
  </Section>
);
