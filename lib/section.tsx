import { Logo, Objective, Section } from "@stembord/react-document";
import * as React from "react";
import logo from "./logo.png";
import section1 from "./sections/1";
import section2 from "./sections/2";

export default (
  <Section title="Lesson Template">
    <Logo src={logo} />
    <Objective>To Learn</Objective>
    {section1}
    {section2}
  </Section>
);
