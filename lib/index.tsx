import { Node } from "@stembord/nodes";
import { Section } from "@stembord/react-document";
import stembord from "../stembord.json";
import section from "./section";

Node.create(stembord, {
  sectionData: Section.parse(section.props)
});
