const gulp = require("gulp"),
  ParcelBundler = require("parcel-bundler"),
  del = require("del");

/* Build 
================================================ */
const cleanCache = () => del(["./.cache"]);

gulp.task("clean-cache", cleanCache);

const clean = () => del(["./src"]);

gulp.task("clean", clean);

const parcel = () =>
  new ParcelBundler("./lib/index.tsx", {
    outDir: "./src",
    outFile: "index.js",

    publicUrl: "@PUBLIC_URL@",
    contentHash: true,

    cache: true,
    cacheDir: ".cache",
    minify: true,

    hmr: true,
    watch: false,

    target: "browser",
    https: false,

    logLevel: 3
  }).bundle();

gulp.task("parcel", parcel);

const build = gulp.series(clean, parcel);

gulp.task("build", build);
gulp.task("default", build);
