import { Node } from "@stembord/nodes";
import "katex/dist/katex.min.css";
import { render } from "react-dom";
import "../../lib";
import stembord from "../../stembord.json";

const {
  data: { section }
} = Node.get(stembord.name);

render(section, document.getElementById("app"));
